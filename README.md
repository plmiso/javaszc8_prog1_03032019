

Na rozgrzewkę

1) Napisz metodę, która sprawdza pełnoletność danej osoby. Zastanów się nad sygnaturą metody, następnie ją zaimplementuj i uruchom dla kilku wartości. 
2) Napisz program, który rysuje choinkę ze znaczków '*', tj. trójkąt równoramienny a pod nim prostokąt. 
3) Dana jest tablica liczb całkowitych, wypisz: 
a) wszystkie po kolei 
b) wszystkie od końca 
c) wszystkie na nieparzystych pozycjach 
d) wszystkie podzielne przez 3 
e) sumę wszystkich 
f) sumę pierwszych 4 
g) sumę ostatnich 5 większych od 2 
h) ilość liczb idąc od początku tablicy, by ich suma przekroczyła 10 
4) Napisz program który wypisze spośród trzech kolejno podanych liczb wypisze największą.
5) Napisz program, który wypisze średnią i sumę trzech podanych kolejno liczb
6) Napisz program, który będzie wyliczał stopę zwrotu z inwestycji, danymi wejścia będą:
- wartość inwestycji
- roczne oprocentowanie
- czas inwestycji w latach
a*) przerób program tak, aby przyjmował informację o tym czy oprocentowanie jest w skali roku/miesiąca/tygodnia/dnia i wyliczał oprocentowanie na tej podstawie
7)Napisz program, który sprawdzi czy dany rok jest przestępny lub nie

Na rozgrzewkę plus

1) Napisz program, który wczytuje z wejścia ciąg liczb i wypisuje go w odwróconej kolejności.
2) Napisz program, który wczytuje z wejścia cyfrę i wypisuje dla niej odpowiednie słowo
a) przerób program tak aby przyjmował wraz z cyfrą język, w którym powinien wyświetlić cyfrę (polski, angielski, niemiecki), zastanów się nad możliwie najefektywniejszym rozwiązaniem
b) przerób program tak, aby zmienna dotycząca wersji językowej była podawana tylko raz i nazwy cyfr wyświetlały się w określonym języku dopóki po raz kolejny nie zmienimy języka

3) Napisz program, który w 10-elementowej tablicy symuluje działanie stosu. Na początku stos jest pusty, a następnie ma się zapełniać lub opróżniać zgodnie z wczytanymi z wejścia poleceniami.
-Na wejście programu podana zostanie pewna nieokreślona liczba zestawów danych. Zestawy składają się albo z jednej linii zawierającej znak - (polecenie zdjęcia liczby ze stosu i wypisania jej na wyjście), albo dwóch linii, z których pierwsza zawiera znak + (polecenie wstawienia liczby na stos), a druga niewielką liczbę całkowitą. Poszczególne zestawy zostaną rozdzielone znakiem nowej linii.
Na wyjściu programu ma się pojawić ciąg napisów będących rezultatem wykonania pojawiających się na wejściu poleceń (jeżeli polecenie udało się wykonać, to wypisujemy jego rezultat: w przypadku wstawienia liczby na stos wypisujemy ':)'; w przypadku zdjęcia liczby jej wartość; w przypadku błędu ':('). Poszczególne napisy należy rozdzielić znakami nowej linii.

Zadania - wprowadzenie do algorytmiki
1) CEZAR

Szyfr Cezara jest to szyfr za pomocą, którego Juliusz Cezar szyfrował swoje listy do Cycerona. Jako ciekawostkę można podać, że szyfr ten był podobno używany jeszcze w 1915 roku w armii rosyjskiej, gdyż tylko tak prosty szyfr wydawał się zrozumiały dla sztabowców.

Każdą literę tekstu jawnego zamieniamy na literę przesuniętą o 3 miejsca w prawo. I tak literę A szyfrujemy jako literę D, literę B jako E itd. W przypadku litery Z wybieramy literę C. W celu odszyfrowania tekstu powtarzamy operację tym razem przesuwając litery o 3 pozycje w lewo.
Input

Na wejściu pojawi się tekst zawierający jedynie wielkie litery alfabetu łacińskiego, spacje oraz znaki nowej linii, a jego długość nie przekracza 200 znaków.
Output

Na wyjściu otrzymujemy zaszyfrowany tekst używając Szyfru Cezara.
Example

Input:
ABC DEF
TERA EST ROTUNDA

Output:
DEF GHI
WHUD HVW URWXQGD

2) SAMOLOT

Bajtockie Linie Lotnicze wzbogaciły swoją flotę o nowy model samolotu. W samolocie tym jest n1 rzędów miejsc siedzących w klasie biznesowej oraz n2 rzędów w klasie ekonomicznej. W klasie biznesowej każdy rząd ma k1 miejsc siedzących, a w klasie ekonomicznej — k2 miejsc.
Zadanie
Napisz program, który:
wczyta informacje na temat dostępnych miejsc siedzących w samolocie,
wyznaczy sumaryczną liczbę wszystkich miejsc siedzących,
wypisze wynik
Wejście

W pierwszym i jedynym wierszu wejścia znajdują się cztery liczby naturalne n1, k1, n2, i k2 (1<=n1,k1,n2,k2<=1000), pooddzielane pojedynczymi odstępami.
Wyjście

Pierwszy i jedyny wiersz wyjścia powinien zawierać jedną liczbę całkowitą - liczbę miejsc siedzących w analizowanym samolocie.
Przykład

Wejście
2 5 3 20

Wyjście
70

3) ADRES EMAIL 

Bitek dopiero poznaje tajniki tworzenia stron WWW, a jego wuj Bajtosław bardzo mu w tym pomaga. Dziś mają za zadanie stworzyć witrynę obsługującą sklep internetowy. Jak zwykle obaj webmasterzy dzielą się pracą. Bitek zajmuje się częścią programistyczną, zaś jego wuj częścią wizualną. Najtrudniejszą rzeczą będzie zaimplementowanie formularza kontaktowego, w którym nasz bohater musi stworzyć walidację adresu e-mail - nie wie jeszcze, że HTML 5 ma wbudowany mechanizm walidacji adresów mailowych. Czasu jest niewiele, a Bitek ma spore problemy, żeby to poprawnie zaprogramować - szczególnie dlatego, że musi używać języka PHP. Pomóż naszemu bohaterowi i napisz program w dowolnym języku sprawdzający poprawność adresu e-mail. Oto kryteria:

adres e-mail powinien

    zawierać dokładnie jeden znak (@)
    można używać małych lub dużych liter języka łacińskiego, cyfr oraz znaki:
        kropka (.)
        podłoga (_)
    adres e-mail musi mieć format [pierwszy ciąg znaków]@[drugi ciąg znaków].[trzeci ciąg znaków składający się z 2 lub 3 liter] (pierwszy i drugi ciąg znaków musi się składać z n znaków, gdzie n zawiera się w przedziale [1..20],
    w mailu może być wiele znaków kropki i każda musi znajdować się między dwoma znakami różnymi niż znak (.) i (@) 

Wejście

W pierwszym wierszu jedna liczba t określająca liczbę zestawów danych.

Każdy zestaw składa się z jednego wiersza, w którym mogą wystąpić znaki ASCII z przedziału [32..126]. Długość wiersza nie przekracza 100 znaków. Na początku i końcu każdego wiersza nie występują spacje.
Wyjście

Dla każdego zestawu testowego napis Tak, jeśli adres e-mail jest prawidłowy oraz Nie w przeciwnym razie.
Przykład

Wejście:
5
mat h@edu.pl
algorytm@edu.pl
algoliga@algoliga.edu.pl
1234@123.PL
1234@123..pl

Wyjście:
Nie
Tak
Tak
Tak
Nie

4) PODCIĄG


Jak to mówią "Reklama dźwignią handlu!". Skoro tu jesteś, to przemówiła do ciebie nazwa zadania i masz nadzieję, że jest ona prawdziwa. Niestety, reklamy i spoty nie zawsze mówią prawdę i często czujemy się oszukani przez system. Czytając to zastanawiasz się, czy autor jest uczciwy i rzeczywiście umieścił łatwe zadanie, czy tylko chciał, aby ilość odwiedzin tej strony była większa? Przekonaj się sam czytając treść zadania:

Sprawdź, czy istnieje taki podciąg (niekoniecznie spójny) pierwszego ciągu, który jest równoważny drugiemu ciągowi.
Wejście

W pierwszym wierszu liczba t określająca ilość zestawów danych, gdzie t < 105.

Każdy zestaw składa się z dwóch ciągów wielkich liter języka łacińskiego. Każdy ciąg ma co najmniej jeden i co najwyżej 103 znaków.
Wyjście

Dla każdego zestawu wypisz "Tak", jeśli z pierwszego ciągu można wybrać taki podciąg, który jest równy drugiemu ciągowi lub napis "Nie" w przeciwnym wypadku.
Przykład

Wejście:
4
ALGOLIGA GOLA
LATWEZADANIE LATANIE
SSAK KAS
ALAMA ALAMAKOTA

Wyjście:
Tak
Tak
Nie
Nie

5) WERYFIKUJEMY PESEL



Jan Kowalski musi wpisać do systemu szpitalnego dane osobowe pacjenta, oprócz imienia i nazwiska musi również wpisać PESEL pacjenta. Jakież było jego zdziwienie, gdy spostrzegł, że pewnych pacjentów system nie przyjmował z powodu wadliwego PESELu.

Twoim zadaniem jest sprawdzenie, czy podana liczba 11-cyfrowa jest poprawnym PESELem.

Aby sprawdzić czy dany PESEL jest prawidłowy należy wykonać następujące działania:

Pierwszą cyfrę mnożymy przez 1,
drugą cyfrę mnożymy przez 3,
trzecią cyfrę mnożymy przez 7,
czwarta cyfrę mnożymy przez 9,
piątą cyfrę mnożymy przez 1,
szóstą cyfrę mnożymy przez 3,
siódmą cyfrę mnożymy przez 7,
ósmą cyfrę mnożymy przez 9,
dziewiątą cyfrę mnożymy przez 1,
dziesiątą cyfrę mnożymy przez 3,
jedenastą cyfrę mnożymy przez 1.

Tak uzyskane 11 iloczynów dodajemy do siebie. Jeśli ostatnia cyfra tej sumy jest zerem to podany PESEL jest prawidłowy. Przykład dla numeru PESEL 44051401458

4*1 + 4*3 + 0*7 + 5*9 + 1*1 + 4*3 + 0*7 + 1*9 + 4*1 + 5*3 + 8*1 = 4 + 12 + 0 + 45 + 1 + 12 + 0 + 9 + 4 + 15 + 8 = 110

Źródło: www.wikipedia.pl

Jeśli suma jest większa od zera, wtedy sprawdzamy jej poprawność. W przeciwnym przypadku nr PESEL jest błędny. Ponieważ ostatnia cyfra liczby 110 jest zerem więc podany PESEL jest prawidłowy.

Na wejściu podana jest w pojedyńczej linii ilość t<=100 numerów PESEL do sprawdzenia. W kolejnych t liniach są 11-cyfrowe liczby.
Output

W pojedyńczej linii powinna zostać wyświetlona litera D, jeśli numer PESEL jest poprawny lub N, gdy nie.
Example

Input:
2
44051401458
12345678901

Output:
D
N



5) STAN I JEGO MANHATAN

Stan Kowalsky od dawna pracuje jako taksówkarz na Manhattanie i "wozi" swoich pasażerów pomiędzy różnymi miejscami w przestrzeni k-wymiarowej. Lot z jednego punktu do drugiego składa się z pojedynczych skoków teleportacyjnych, takich, w których położenie taksówki zmienia się tylko w jednym wymiarze. Zakładając na przykład, że Stan porusza się w przestrzeni pięciowymiarowej, to aby przenieść się z punktu o współrzędnych (3, 1, 8, 1, 2) do punktu (3, 7, 8, 1, 0), musi wykonać dwa skoki. Pierwszy z nich musi nastąpić do punktu (3, 7, 8, 1, 2) albo do punktu (3, 1, 8, 1, 0). Niezależnie od tego, którą trasę wybierze, odległość, którą pokona wynosi 8 jednostek.

Stan każdego dnia rozpoczyna pracę w bazie, która znajduje się w punkcie o wszystkich współrzędnych zerowych. Następnie przewozi swoich pasażerów latając po kolei do punktów, o określonych współrzędnych. Na koniec wraca do bazy.

Taksówkarz po zakończonej pracy chciałby wiedzieć, jaki najdłuższy lot odbył w danym dniu oraz ile pojedynczych skoków musiał wykonać.
Wejście

W pierwszej linii liczba przypadków testowych (dni) t (t ≤ 100).

W pierwszej linii pojedynczego testu dwie liczby całkowite: liczba wymiarów przestrzeni k (1 ≤ k ≤ 1000) oraz liczba punktów, do których kolejno latał taksówkarz n (1 ≤ n ≤ 100).
W każdej z kolejnych n linii po k liczb całkowitych xi dla i=1..k (-109 ≤ xi ≤ 109).
Wyjście

Dla każdego testu w osobnej linii dwie liczby całkowite. Najpierw największa odległość pokonana w czasie jednego lotu (uwzględniając lot z bazy do pierwszego punktu i lot z ostatniego punktu do bazy). Następnie liczba pojedynczych skoków teleportacyjnych wykonanych w danym dniu.
Przykład

Wejście:
1
5 2
3 1 8 1 2
3 7 8 1 0

Wyjście:
19 11


6)FIBONACCI

Napisz program, który wypisze ciąg Fibonacciego do podanego na wejściu elementu ciągu

*) RODZINKA 

Wesoła rodzinka Państwa Bajtockich wybrała się w góry. Właśnie wędrują sobie gęsiego po śniegu. Pierwszy idzie mały Bitek, następnie jego mama Bajbitka a na końcu ojciec Bajtjusz. 

Zadanie: napisz program, który określi, ile razy na odcinku s metrów ślady wszystkich trzech osób pokryją się.

Zakładamy, że wszyscy ruszają z tego samego miejsca, pierwszy ślad jest tuż przed linią startu oraz wielkość śladu jest pomijalnie mała.
Wejście

W pierwszym i jedynym wierszu cztery liczby całkowite a, b, c oraz s, gdzie a, b i c to długości kroków, jakie stawiają osoby z rodzinki (w centymetrach), następnie liczba całkowita s, określająca długość trasy (w metrach), wszystkie te liczby zawierają się w przedziale: [1..1010]. (dane są tak dobrane, że do obliczeń wystarczą typy 64 bitowe)
Wyjście

Jedna liczba określająca ile razy pokryją się ślady wszystkich osób w rodzinie.
Przykład 1

Wejście:
30 40 50 2

Wyjście:
0

Przykład 2

Wejście:
30 30 60 3

Wyjście:
5

NA KONIEC!

-UPORZĄDKUJ KOD WSZYSTKICH ZADAŃ
-DODAJ JEDNOZDANIOWE KOMENTARZE OPISUJĄCE FUNKCJONALNOŚCI
-PRZYGOTUJ REPOZYTORIUM LOKALNE (OSOBNY BRANCH NA ZADANIE, BRANCH DEVELOP POWINIEN ZAWIERAĆ WSZYSTKIE ZADANIA )
- WYPCHNIJ WSZYSTKIE ZADANIA DO REPOZYTORIUM ZDALNEGO (BRANCH: DEVELOP)
-WYSTAW MERGE REQUESTA DO BRANCHA MASTER, DODAJ PROWADZACEGO JAKO "ASIGNEE" I DWÓCH KOLEGÓW JAKO "REVIEWER"
-OCZEKUJ KODE REVIEW, A W NIM UWAG DO KODU:)


